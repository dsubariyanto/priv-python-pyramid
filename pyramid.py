#!/usr/bin/python

num = int(input("Enter the number of rows:"))

#define template
pola = "x o "
pola2 = "o x "

#define mengali. base1 untuk piramid tengah, base2 untuk berapa kali pola terulang, base3 untuk sisa setelah pola terulang
base1 = num * 2 - 1
base2 = base1 // 4
base3 = base1 % 4

#print top of pyramid
for d in range (1,num,+1):
  kal = d // 4
  sis = d % 4
  print((num - 1) * ' ' + (kal * pola + pola[0:sis:])[::-1])
  
#print middle of pyramid
print((base2 * pola + pola[0:base3])[::-1])

#print bottom of pyramid
for e in range (num-1,0,-1):
  kal = e // 4
  sis = e % 4
  #determine is it odd or even number
  if (num % 2) == 0:
    print((num-e) * ' ' + (kal * pola2 + pola2[0:sis]))
  else:
    print((num-e) * ' ' + (kal * pola + pola[0:sis]))