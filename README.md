# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This program for generate required pattern on exam. This is simple task

When it input 1 the output should be:
x

When it input 2 the output should be:
 x
o x
 o
 
When it input 5 the output should be:
    x
     x
    o x
     o x
x o x o x
 x o 
  x o
   x 
    x
	
When it input 14 the output should be:
             x
              x
             o x
              o x
             x o x
              x o x
             o x o x
              o x o x
             x o x o x
              x o x o x
             o x o x o x
              o x o x o x
             x o x o x o x
o x o x o x o x o x o x o x
 o x o x o x o
  o x o x o x 
   o x o x o x
    o x o x o 
     o x o x o
      o x o x 
       o x o x
        o x o 
         o x o
          o x 
           o x
            o 
             o
			 
and so on...